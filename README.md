# README #

Kris Denton Tic-Tac-Toe game. 

## How To Play ##

### Windows Command Line ###
    * Navigate to the src\main\java directory in the project in CMD
    * Type in `play.bat` and press enter

### MINGW Terminal ###
    * Navigate to the src\main\java directory in the project in MINGW terminal
    * Type in `./play.bat` and press enter