import java.util.List;

public class BoardManager implements ManagesBoard{
    private Board board;

    public BoardManager(Board board) {
        this.board = board;
    }

    @Override
    public String generateBoard() {
        List<List<Cell>> rows = board.getRows();
        StringBuilder sb  = new StringBuilder();

        sb.append("\n   |   |   \n");
        for (int i = 0; i < rows.size(); i++) {
            if (i != 0) {
                sb.append("___|___|___\n");
                sb.append("   |   |   \n");
            }

            List<Cell> row = rows.get(i);
            StringBuilder set = new StringBuilder(" ");

            for (int j = 0; j < row.size(); j++) {
                if (j != 0) {
                    set.append("| ");
                }

                set.append(row.get(j).getValue()).append(" ");
            }
            set.append("\n");
            sb.append(set);
        }
        sb.append("   |   |   \n");

        return sb.toString();
    }
}
