class GameRunner {
    private PlaysGame game;

    GameRunner(PlaysGame game){
        this.game = game;
    }

    void runGame() {
        while(!this.game.gameIsOver()) {
            this.game.play();
        }
    }
}
