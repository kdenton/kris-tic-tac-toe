class Cell {
    private int x;
    private int y;
    String value;

    int getX() {
        return this.x;
    }

    void setX(int x) {
        this.x = x;
    }

    int getY() {
        return this.y;
    }

    void setY(int y) {
        this.y = y;
    }

    String getValue() {
        return this.value;
    }

    void setValue(String value) {
        this.value = value;
    }
}
