import java.io.InputStream;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Reader implements ReadsInput {
    private Scanner scanner;
    private PresentsOutput presenter;

    Reader(InputStream input, PresentsOutput presenter) {
        System.setIn(input);
        this.scanner = new Scanner(System.in);
        this.presenter = presenter;
    }

    @Override
    public int readIntInput() {
        try {
            return scanner.nextInt();
        }
        catch (InputMismatchException e) {
            presenter.displayOutput(Messages.WrongInputForId);
            scanner.nextLine();
            return -1;
        }
    }
}
