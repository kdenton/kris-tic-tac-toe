public interface PlaysGame {
    void play();
    boolean gameIsOver();
    boolean gameIsWon();
    boolean validPlacement(int cellIndex);
}
