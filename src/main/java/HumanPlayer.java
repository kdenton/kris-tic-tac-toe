public class HumanPlayer implements Player {
    private String symbol;
    private Board board;
    private ReadsInput reader;

    HumanPlayer(String symbol, Board board, ReadsInput reader) {
        this.symbol = symbol;
        this.board = board;
        this.reader = reader;
    }

    @Override
    public String getPlayerSymbol() {
        return this.symbol;
    }

    @Override
    public int decideNextMove() {
        int input = reader.readIntInput();

        if (input != - 1){
            return input - 1;
        }

        return input;
    }

    @Override
    public void makeNextMove(int cellToReplace) {
        board.updateCell(cellToReplace, getPlayerSymbol());
    }
}
