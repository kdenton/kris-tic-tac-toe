class Messages {
    static String WrongInputForId = "\nPlease enter an integer for the cell ID.";
    static String GameIsOver = "\nGame is over.";
    static String PlayerTurnPrompt(String symbol) {
        return "\nPlayer " + symbol
                + "'s Turn\nEnter cell to place your " + symbol + ": ";
    }
    static String CellIdOutOfBounds = "\nPlease enter a cell between 1 and 9.\n";
    static String CellTaken = "\nPlease enter an unused cell.\n";
    static String GameIsTied = "\nThe game is tied!\n";
    static String GameIsWon(String currentPlayerSymbol) {
        return "\nThe " + currentPlayerSymbol + " player has won the game!";
    }
}
