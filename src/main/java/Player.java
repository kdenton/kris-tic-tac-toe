interface Player {
    String getPlayerSymbol();
    int decideNextMove();
    void makeNextMove(int cellToReplace);
}
