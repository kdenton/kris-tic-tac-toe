import java.io.OutputStream;
import java.io.PrintStream;

public class Presenter implements PresentsOutput{
    Presenter(OutputStream output) {
        System.setOut(new PrintStream(output));
    }

    @Override
    public void displayOutput(String toDisplay) {
        System.out.print(toDisplay);
    }
}
