import java.util.List;

public class Game implements PlaysGame {
    private Board board;
    private PresentsOutput presenter;
    private Player playerOne;
    private Player playerTwo;
    private Player currentPlayer;
    private ManagesBoard boardManager;

    Game(Board board, PresentsOutput presenter, Player playerOne, Player playerTwo, ManagesBoard boardManager) {
        this.board = board;
        this.presenter = presenter;
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.currentPlayer = playerTwo;
        this.boardManager = boardManager;
    }

    @Override
    public void play() {
        int cellToReplace = -1;
        boolean tryAgain = true;
        currentPlayer = currentPlayer == playerOne ? playerTwo : playerOne;

        while (tryAgain) {
            presenter.displayOutput(boardManager.generateBoard());
            presenter.displayOutput(Messages.PlayerTurnPrompt(currentPlayer.getPlayerSymbol()));

            cellToReplace = currentPlayer.decideNextMove();

            if (cellToReplace != -1 && validPlacement(cellToReplace)) {
                tryAgain = false;
            }
        }

        currentPlayer.makeNextMove(cellToReplace);
    }

    @Override
    public boolean gameIsOver() {
        if (gameIsWon()) {
            presenter.displayOutput(boardManager.generateBoard());
            presenter.displayOutput(Messages.GameIsWon(currentPlayer.getPlayerSymbol()));
            return true;
        }

        boolean foundUnusedCell = false;
        for (Cell c:board.getBoard()) {
            if ((!c.getValue().equals(playerOne.getPlayerSymbol()) &&
                    !c.getValue().equals(playerTwo.getPlayerSymbol()))){
                foundUnusedCell = true;
                break;
            }
        }

        if (!foundUnusedCell) {
            presenter.displayOutput(boardManager.generateBoard());
            presenter.displayOutput(Messages.GameIsTied);
            return true;
        }

        return false;
    }

    @Override
    public boolean gameIsWon() {
        return checkRowWin() || checkColumnWin() || checkDiagonalWin();

    }

    private boolean checkRowWin() {
        List<List<Cell>> rows = board.getRows();

        for (List<Cell> row : rows) {
            if (row.stream().allMatch(x -> x.value.equals(row.get(0).value))) {
                return true;
            }
        }

        return false;
    }

    private boolean checkColumnWin() {
        List<List<Cell>> columns = board.getColumns();

        for (List<Cell> column : columns) {
            if (column.stream().allMatch(x -> x.value.equals(column.get(0).value))) {
                return true;
            }
        }

        return false;
    }

    private boolean checkDiagonalWin() {
        List<List<Cell>> diagonals = board.getDiagonals();

        for (List<Cell> diagonal : diagonals) {
            if (diagonal.stream().allMatch(x -> x.value.equals(diagonal.get(0).value))) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean validPlacement(int cellIndex) {
        if (cellIndex < 0 || cellIndex > 8) {
            presenter.displayOutput(Messages.CellIdOutOfBounds);
            return false;
        }

        Cell cell = board.getBoardCell(cellIndex);
        if (cell.getValue().equals(playerOne.getPlayerSymbol()) || cell.getValue().equals(playerTwo.getPlayerSymbol())) {
            presenter.displayOutput(Messages.CellTaken);
            return false;
        }

        return true;
    }
}
