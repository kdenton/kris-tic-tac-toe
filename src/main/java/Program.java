import java.util.LinkedList;
import java.util.List;

public class Program {
    public static void main(String[] args){
        List<Cell> cells = new LinkedList<>();
        int counter = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                counter++;
                Cell cellToAdd = new Cell();
                cellToAdd.setValue(Integer.toString(counter));
                cellToAdd.setX(i);
                cellToAdd.setY(j);

                cells.add(cellToAdd);
            }
        }

        Board board = new Board(cells);
        ManagesBoard boardManager = new BoardManager(board);
        PresentsOutput presenter = new Presenter(System.out);
        ReadsInput reader = new Reader(System.in, presenter);
        Player playerOne = new HumanPlayer("O", board, reader);
        Player playerTwo = new ComputerPlayer("X", "O", board);

        Game game = new Game(board, presenter, playerOne, playerTwo, boardManager);
        GameRunner runner = new GameRunner(game);

        runner.runGame();
    }
}
