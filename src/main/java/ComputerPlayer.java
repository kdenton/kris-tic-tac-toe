import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class ComputerPlayer implements Player {
    private String symbol;
    private String opponentSymbol;
    private Board board;
    private Cell nextMove;

    ComputerPlayer(String symbol, String opponentSymbol, Board board) {
        this.symbol = symbol;
        this.opponentSymbol = opponentSymbol;
        this.board = board;
    }

    @Override
    public String getPlayerSymbol() {
        return this.symbol;
    }

    @Override
    public int decideNextMove() {
        if (checkWin() || checkBlock() || checkFork() || checkTwoInRow() || checkBlockFork() ||
                checkCenter() || checkOppositeCorner() || checkEmptyCorner() || checkEmptySide()) {
            return board.getBoard().indexOf(nextMove);
        }

        return -1;
    }

    @Override
    public void makeNextMove(int cellToReplace) {
        board.updateCell(cellToReplace, getPlayerSymbol());
    }

    private void setNextMove(Cell cell) {
        this.nextMove = cell;
    }

    Cell getNextMove() {
        return this.nextMove;
    }

    private String getOpponentSymbol() {
        return this.opponentSymbol;
    }

    private String getOppositeSymbol(String symbol) {
        return symbol.equals(getPlayerSymbol()) ? getOpponentSymbol() : getPlayerSymbol();
    }

    private Board getBoard() {
        return this.board;
    }

    boolean checkWin() {
        return checkRowWin(getPlayerSymbol()) ||
                checkColumnWin(getPlayerSymbol()) ||
                checkDiagonalWin(getPlayerSymbol());
    }

    boolean checkBlock() {
        return checkRowWin(getOpponentSymbol()) ||
                checkColumnWin(getOpponentSymbol()) ||
                checkDiagonalWin(getOpponentSymbol());
    }

    boolean checkFork() {
        List<Cell> possibleCells = getPossibleFork(getPlayerSymbol(), getBoard());

        for (Cell cell: possibleCells) {
            if (Collections.frequency(possibleCells, cell) > 1) {
                setNextMove(cell);
                return true;
            }
        }

        return false;
    }

    boolean checkBlockFork() {
        List<Cell> possibleCells = getPossibleFork(getOpponentSymbol(), getBoard());

        for (Cell cell: possibleCells) {
            if (Collections.frequency(possibleCells, cell) > 1) {
                setNextMove(cell);
                return true;
            }
        }

        return false;
    }

    boolean checkTwoInRow() {
        List<Cell> possibleCells = new LinkedList<>();
        possibleCells.addAll(checkPossibleTwoInRow());
        possibleCells.addAll(checkPossibleTwoInDiagonal());
        possibleCells.addAll(checkPossibleTwoInColumn());

        if (possibleCells.size() > 0) {
            for(Cell cell : possibleCells) {
                int index = getBoard().getBoard().indexOf(cell);
                Board board = getBoard().deepClone();
                board.updateCell(index, getPlayerSymbol());

                List<Cell> possibleForks = getPossibleFork(getOpponentSymbol(), board);

                for(Cell fork : possibleForks) {
                    if (Collections.frequency(possibleForks, fork) > 1) {
                        return false;
                    }
                }
            }

            setNextMove(possibleCells.stream().findFirst().get());
            return true;
        }

        return false;
    }

    boolean checkCenter() {
        List<Cell> boardCells = board.getBoard();
        if (!boardCells.get(4).getValue().equals(getPlayerSymbol()) &&
                !boardCells.get(4).getValue().equals(getOpponentSymbol())) {
            setNextMove(board.getBoard().get(4));
            return true;
        }

        return false;
    }

    boolean checkOppositeCorner() {
        List<List<Cell>> cornerPairs = getCornerPairs();

        for (List<Cell> pair: cornerPairs) {
            if (pair.stream().anyMatch(x -> x.getValue().equals(getOpponentSymbol())) &&
                    pair.stream().anyMatch(x -> !x.getValue().equals(getPlayerSymbol()) &&
                            !x.getValue().equals(getOpponentSymbol()))) {
                setNextMove(pair.stream().filter(x -> !x.getValue().equals(getPlayerSymbol()) &&
                !x.getValue().equals(getOpponentSymbol())).findFirst().get());
                return true;
            }
        }

        return false;
    }

    boolean checkEmptyCorner() {
        List<List<Cell>> cornerPairs = getCornerPairs();

        for (List<Cell> pair: cornerPairs) {
            if (pair.stream().anyMatch(x -> !x.getValue().equals(getPlayerSymbol()) &&
                    !x.getValue().equals(getOpponentSymbol()))) {
                setNextMove(pair.stream().filter(x -> !x.getValue().equals(getPlayerSymbol()) &&
                        !x.getValue().equals(getOpponentSymbol())).findFirst().get());
                return true;
            }
        }

        return false;
    }

    boolean checkEmptySide() {
        List<Cell> middles = getMiddleSides();

        if (middles.stream().anyMatch(x -> !x.getValue().equals(getPlayerSymbol()) &&
                !x.getValue().equals(getOpponentSymbol()))) {
            setNextMove(middles.stream().filter(x -> !x.getValue().equals(getPlayerSymbol()) &&
                    !x.getValue().equals(getOpponentSymbol())).findFirst().get());
            return true;
        }

        return false;
    }

    boolean checkColumnWin(String symbol) {
        List<List<Cell>> columns = board.getColumns();

        for (List<Cell> column : columns) {
            if (column.stream().filter(x -> !x.getValue().equals(symbol)).count() == 1) {
                Cell cell = column.stream().filter(x -> !x.getValue().equals(symbol)).findFirst().get();
                if (!cell.getValue().equals(getOppositeSymbol(symbol))) {
                    setNextMove(cell);
                    return true;
                }
            }
        }

        return false;
    }

    boolean checkRowWin(String symbol) {
        List<List<Cell>> rows = board.getRows();

        for (List<Cell> row : rows) {
            if (row.stream().filter(x -> !x.getValue().equals(symbol)).count() == 1) {
                Cell cell = row.stream().filter(x -> !x.getValue().equals(symbol)).findFirst().get();
                if (!cell.getValue().equals(getOppositeSymbol(symbol))) {
                    setNextMove(cell);
                    return true;
                }
            }
        }

        return false;
    }

    boolean checkDiagonalWin(String symbol) {
        List<List<Cell>> diagonals = board.getDiagonals();

        for (List<Cell> diagonal : diagonals) {
            if (diagonal.stream().filter(x -> !x.getValue().equals(symbol)).count() == 1) {
                Cell cell = diagonal.stream().filter(x -> !x.getValue().equals(symbol)).findFirst().get();
                if (!cell.getValue().equals(getOppositeSymbol(symbol))) {
                    setNextMove(cell);
                    return true;
                }
            }
        }

        return false;
    }

    private List<Cell> getPossibleFork(String symbol, Board board) {
        List<List<List<Cell>>> paths = getAllPaths(board);
        List<Cell> possibleCells = new LinkedList<>();

        for (List<List<Cell>> path: paths) {
            for (List<Cell> group: path) {
                if (group.stream().noneMatch(x -> x.getValue().equals(getOppositeSymbol(symbol)))
                        && group.stream().anyMatch(x -> x.getValue().equals(symbol))) {
                    if (group.stream().filter(x -> !x.getValue().equals(symbol)).count() == 2) {
                        List<Cell> foundCells = group.stream().filter(
                                x -> !x.getValue().equals(symbol)).collect(Collectors.toList());
                        possibleCells.addAll(foundCells);
                    }
                }
            }
        }

        return possibleCells;
    }

    private List<List<Cell>> getCornerPairs() {
        List<List<Cell>> cornerPairs = new LinkedList<>();
        List<Cell> boardCells = board.getBoard();

        List<Cell> firsPair = new LinkedList<>();
        firsPair.add(boardCells.stream().filter(
                x -> x.getX() == 0 && x.getY() == 0).findFirst().get());
        firsPair.add(boardCells.stream().filter(
                x -> x.getX() == board.getRowColumnCount() - 1 &&
                        x.getY() == board.getRowColumnCount() - 1).findFirst().get());
        cornerPairs.add(firsPair);

        List<Cell> secondPair = new LinkedList<>();
        secondPair.add(boardCells.stream().filter(
                x -> x.getX() == board.getRowColumnCount() - 1 && x.getY() == 0).findFirst().get());
        secondPair.add(boardCells.stream().filter(
                x -> x.getX() == 0 && x.getY() == board.getRowColumnCount() - 1).findFirst().get());
        cornerPairs.add(secondPair);

        return cornerPairs;
    }

    private List<Cell> getMiddleSides() {
        List<Cell> boardCells = board.getBoard();
        List<Cell> middles = new LinkedList<>();
        int middleIndex = (int) Math.ceil(board.getRowColumnCount() / 2);

        Cell firstMiddle = boardCells.stream().filter(
                x -> x.getX() == 0 && x.getY() == middleIndex).findFirst().get();
        middles.add(firstMiddle);

        Cell secondMiddle = boardCells.stream().filter(
                x -> x.getX() == middleIndex && x.getY() == 0).findFirst().get();
        middles.add(secondMiddle);

        Cell thirdMiddle = boardCells.stream().filter(
                x -> x.getX() == board.getRowColumnCount() - 1 && x.getY() == middleIndex).findFirst().get();
        middles.add(thirdMiddle);

        Cell fourthMiddle = boardCells.stream().filter(
                x -> x.getX() == middleIndex && x.getY() == board.getRowColumnCount() - 1).findFirst().get();
        middles.add(fourthMiddle);

        return middles;
    }

    private List<List<List<Cell>>> getAllPaths(Board board) {
        List<List<List<Cell>>> paths = new LinkedList<>();
        paths.add(board.getColumns());
        paths.add(board.getRows());
        paths.add(board.getDiagonals());

        return paths;
    }

    List<Cell> checkPossibleTwoInRow() {
        List<List<Cell>> rows = board.getRows();
        List<Cell> possibleCells = new LinkedList<>();

        for (List<Cell> row : rows) {
            if (row.stream().noneMatch(x -> x.getValue().equals(getOpponentSymbol()))) {
                if (row.stream().filter(x -> x.getValue().equals(getPlayerSymbol())).count() == 1) {
                    Cell cell = row.stream().filter(
                            x -> x.getValue().equals(getPlayerSymbol())).findFirst().get();
                    List<Cell> foundCells = row.stream().filter(
                            x -> x.getY() == cell.getY() - 1 ||
                                    x.getY() == cell.getY() + 1).collect(Collectors.toList());
                    possibleCells.addAll(foundCells);
                }
            }
        }

        return possibleCells;
    }

    List<Cell> checkPossibleTwoInColumn() {
        List<List<Cell>> columns = board.getColumns();
        List<Cell> possibleCells = new LinkedList<>();

        for (List<Cell> column : columns) {
            if (column.stream().noneMatch(x -> x.getValue().equals(getOpponentSymbol()))) {
                if (column.stream().filter(x -> x.getValue().equals(getPlayerSymbol())).count() == 1) {
                    Cell cell = column.stream().filter(
                            x -> x.getValue().equals(getPlayerSymbol())).findFirst().get();
                    List<Cell> foundCells = column.stream().filter(
                            x -> x.getX() == cell.getX() - 1 ||
                                    x.getX() == cell.getX() + 1).collect(Collectors.toList());
                    possibleCells.addAll(foundCells);
                }
            }
        }

        return possibleCells;
    }

    List<Cell> checkPossibleTwoInDiagonal() {
        List<List<Cell>> diagonals = board.getDiagonals();
        List<Cell> possibleCells = new LinkedList<>();

        List<Cell> leftDiagonal = diagonals.get(0);
        if (leftDiagonal.stream().noneMatch(x -> x.getValue().equals(getOpponentSymbol()))) {
            if (leftDiagonal.stream().filter(x -> x.getValue().equals(getPlayerSymbol())).count() == 1) {
                Cell cell = leftDiagonal.stream().filter(
                        x -> x.getValue().equals(getPlayerSymbol())).findFirst().get();
                List<Cell> foundCells = leftDiagonal
                        .stream()
                        .filter(
                        x -> x.getX() == cell.getX() - 1  && x.getY() == cell.getY() -1 ||
                                x.getX() == cell.getX() + 1 && x.getY() == cell.getY() + 1)
                        .collect(Collectors.toList());
                possibleCells.addAll(foundCells);
            }
        }

        List<Cell> rightDiagonal = diagonals.get(1);
        if (rightDiagonal.stream().noneMatch(x -> x.getValue().equals(getOpponentSymbol()))) {
            if (rightDiagonal.stream().filter(x -> x.getValue().equals(getPlayerSymbol())).count() == 1) {
                Cell cell = rightDiagonal.stream().filter(
                        x -> x.getValue().equals(getPlayerSymbol())).findFirst().get();
                List<Cell> foundCells = rightDiagonal.stream().filter(
                        x -> x.getX() == cell.getX() - 1 && x.getY() == cell.getY() + 1 ||
                                x.getX() == cell.getX() + 1 && x.getY() == cell.getY() - 1)
                        .collect(Collectors.toList());
                possibleCells.addAll(foundCells);
            }
        }

        return possibleCells;
    }
}
