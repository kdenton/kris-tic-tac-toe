import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

class Board {
    private List<Cell> board;
    private int rowColumnCount;

    Board(List<Cell> board) {
        this.board = board;
        this.rowColumnCount = (int) Math.sqrt(getBoard().size());
    }

    void updateCell(int index, String newValue) {
        Cell cellToUpdate = this.board.get(index);
        cellToUpdate.setValue(newValue);
        this.board.set(index, cellToUpdate);
    }

    List<Cell> getBoard() {
        return this.board;
    }

    Cell getBoardCell(int index) {
        return getBoard().get(index);
    }

    int getRowColumnCount() {
        return this.rowColumnCount;
    }

    List<List<Cell>> getColumns() {
        List<List<Cell>> columns = new LinkedList<>();

        for (int i = 0; i < getRowColumnCount(); i++) {
            int finalI = i;
            List<Cell> column = this.board
                    .stream()
                    .filter(x -> x.getY() == finalI)
                    .collect(Collectors.toList());

            columns.add(column);
        }

        return columns;
    }

    List<List<Cell>> getRows() {
        List<List<Cell>> rows = new LinkedList<>();

        for (int i = 0; i < getRowColumnCount(); i++) {
            int finalI = i;
            List<Cell> row = this.board
                    .stream()
                    .filter(x -> x.getX() == finalI)
                    .collect(Collectors.toList());

            rows.add(row);
        }

        return rows;
    }

    List<List<Cell>> getDiagonals() {
        List<List<Cell>> diagonals = new LinkedList<>();

        List<Cell> diagonal = this.board
                .stream()
                .filter(x -> x.getX() == x.getY())
                .collect(Collectors.toList());
        diagonals.add(diagonal);

        List<Cell> otherDiagonal = new LinkedList<>();
        int j = 0;
        for (int i = getRowColumnCount() - 1; i >= 0; i--) {
            int finalI = i;
            int finalJ = j;
            Cell cell = this.board
                    .stream()
                    .filter(x -> x.getX() == finalI && x.getY() == finalJ)
                    .findFirst()
                    .get();

            otherDiagonal.add(cell);
            j++;
        }
        diagonals.add(otherDiagonal);

        return diagonals;
    }

    Board deepClone() {
        List<Cell> boardCells = getBoard();

        List<Cell> newBoardCells = new LinkedList<>();
        for (Cell cell: boardCells) {
            Cell newCell = new Cell();
            newCell.setValue(cell.getValue());
            newCell.setX(cell.getX());
            newCell.setY(cell.getY());

            newBoardCells.add(newCell);
        }

        return new Board(newBoardCells);
    }
}
