import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GameTest {
    @Test
    public void testGameIsOver_IfAllCellsHaveBeenUsedAndNoWinnersReturnsTrue() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> fullBoard = TestBuilderMethods.buildBoardCells();
        fullBoard.get(0).setValue(playerTwoSymbol);
        fullBoard.get(1).setValue(playerTwoSymbol);
        fullBoard.get(2).setValue(playerOneSymbol);
        fullBoard.get(3).setValue(playerOneSymbol);
        fullBoard.get(4).setValue(playerOneSymbol);
        fullBoard.get(5).setValue(playerTwoSymbol);
        fullBoard.get(6).setValue(playerTwoSymbol);
        fullBoard.get(7).setValue(playerTwoSymbol);
        fullBoard.get(8).setValue(playerOneSymbol);

        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        Game game = TestBuilderMethods.buildGame(new Board(fullBoard), null, null, null, presenter, null);

        assertTrue(game.gameIsOver());
        assertEquals(TestBuilderMethods.generateBoardForTest(fullBoard) + Messages.GameIsTied, output.toString());
    }

    @Test
    public void testGameIsOver_IfThereIsAnAvailableCellReturnsFalse() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(2).setValue(playerOneSymbol);
        boardCells.get(3).setValue(playerTwoSymbol);
        boardCells.get(4).setValue(playerTwoSymbol);
        boardCells.get(5).setValue(playerOneSymbol);
        boardCells.get(6).setValue(playerOneSymbol);
        boardCells.get(7).setValue(playerTwoSymbol);
        boardCells.get(8).setValue(playerTwoSymbol);

        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, presenter, null);

        assertFalse(game.gameIsOver());
    }

    @Test
    public void testPlay_PrintsTheCurrentStateOfTheBoardToThePlayer() {
        String playerOneSymbol = "O";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(1).setValue(playerOneSymbol);
        String expectedOutput = TestBuilderMethods.generateBoardForTest(boardCells) +
                Messages.PlayerTurnPrompt(playerOneSymbol);
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, presenter, null);

        game.play();

        assertEquals(expectedOutput, output.toString());
    }

    @Test
    public void testPlay_PrintsPromptForInitialPlayerToAddSymbol() {
        String playerOneSymbol = "O";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, presenter, null);
        String expectedOutput = TestBuilderMethods.generateBoardForTest(boardCells) +
                Messages.PlayerTurnPrompt(playerOneSymbol);

        game.play();

        assertEquals(expectedOutput, output.toString());
    }

    @Test
    public void testPlay_PrintsOutBoardReflectingCellChangeAndPlayerPromptsOnNextTurn() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        Board board = new Board(boardCells);
        InputStream input = new ByteArrayInputStream("5\n1".getBytes());
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        ReadsInput reader = new Reader(input, presenter);

        Game game = TestBuilderMethods.buildGame(board, null, null, null, presenter, reader);
        List<Cell> updatedBoardState = board.deepClone().getBoard();
        updatedBoardState.get(4).setValue(playerOneSymbol);
        String expectedOutput = TestBuilderMethods.generateBoardForTest(boardCells) +
                Messages.PlayerTurnPrompt(playerOneSymbol) +
                TestBuilderMethods.generateBoardForTest(updatedBoardState) +
                Messages.PlayerTurnPrompt(playerTwoSymbol);

        game.play();
        game.play();

        assertEquals(expectedOutput, output.toString());
    }

    @Test
    public void testCheckRowWin_ThreeOfSameSymbolFirstRowReturnsTrue() {
        String playerOneSymbol = "O";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerOneSymbol);
        boardCells.get(2).setValue(playerOneSymbol);

        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertTrue(game.gameIsWon());
    }

    @Test
    public void testCheckRowWin_ThreeOfSameSymbolSecondRowReturnsTrue() {
        String playerOneSymbol = "O";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(3).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(5).setValue(playerOneSymbol);

        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertTrue(game.gameIsWon());
    }

    @Test
    public void testCheckRowWin_ThreeOfSameSymbolThirdRowReturnsTrue() {
        String playerOneSymbol = "O";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(6).setValue(playerOneSymbol);
        boardCells.get(7).setValue(playerOneSymbol);
        boardCells.get(8).setValue(playerOneSymbol);

        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertTrue(game.gameIsWon());
    }

    @Test
    public void testCheckRowWin_NotThreeOfSameSymbolReturnsFalse() {
        String playerOneSymbol = "O";
        String playerTwoSymbol ="X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerTwoSymbol);
        boardCells.get(2).setValue(playerOneSymbol);

        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertFalse(game.gameIsWon());
    }

    @Test
    public void testCheckColumnWin_ThreeOfSameSymbolFirstColumnReturnsTrue() {
        String playerOneSymbol = "O";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(3).setValue(playerOneSymbol);
        boardCells.get(6).setValue(playerOneSymbol);

        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertTrue(game.gameIsWon());
    }

    @Test
    public void testCheckColumnWin_ThreeOfSameSymbolSecondColumnReturnsTrue() {
        String playerOneSymbol = "O";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(1).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(7).setValue(playerOneSymbol);

        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertTrue(game.gameIsWon());
    }

    @Test
    public void testCheckColumnWin_ThreeOfSameSymbolThirdColumnReturnsTrue() {
        String playerOneSymbol = "O";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerOneSymbol);
        boardCells.get(5).setValue(playerOneSymbol);
        boardCells.get(8).setValue(playerOneSymbol);

        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertTrue(game.gameIsWon());
    }

    @Test
    public void testCheckColumnWin_NotThreeOfSameSymbolReturnsFalse() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(6).setValue(playerOneSymbol);
        boardCells.get(7).setValue(playerTwoSymbol);
        boardCells.get(8).setValue(playerOneSymbol);

        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertFalse(game.gameIsWon());
    }

    @Test
    public void testCheckDiagonalWin_ThreeOfSameSymbolBackSlashDiagonalReturnsTrue() {
        String playerOneSymbol = "O";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(8).setValue(playerOneSymbol);

        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertTrue(game.gameIsWon());
    }

    @Test
    public void testCheckDiagonalWin_ThreeOfSameSymbolForwardSlashDiagonalReturnsTrue() {
        String playerOneSymbol = "O";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(6).setValue(playerOneSymbol);

        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertTrue(game.gameIsWon());
    }

    @Test
    public void testCheckDiagonalWin_NotThreeOfSameSymbolReturnsFalse() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerTwoSymbol);
        boardCells.get(8).setValue(playerOneSymbol);

        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertFalse(game.gameIsWon());
    }

    @Test
    public void testValidPlacement_InputLessThanOneReturnsFalseAndPrintsError() {
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, presenter, null);

        assertFalse(game.validPlacement(-1));
        assertEquals(Messages.CellIdOutOfBounds, output.toString());
    }

    @Test
    public void testValidPlacement_InputGreaterThanNineReturnsFalseAndPrintsError() {
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, presenter, null);

        assertFalse(game.validPlacement(9));
        assertEquals(Messages.CellIdOutOfBounds, output.toString());
    }

    @Test
    public void testValidPlacement_ValidInputReturnsTrue() {
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertTrue(game.validPlacement(5));
    }

    @Test
    public void testValidPlacement_CellAlreadyTakenReturnsFalseAndPrintsError() {
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(4).setValue("O");
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, presenter, null);

        assertFalse(game.validPlacement(4));
        assertEquals(Messages.CellTaken, output.toString());
    }

    @Test
    public void testValidPlacement_CellNotTakenReturnsTrue() {
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertTrue(game.validPlacement(1));
    }

    @Test
    public void testGameIsWon_WinningBoardReturnsTrueAndPrintsWinningMessage() {
        String playerOneSymbol = "O";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(8).setValue(playerOneSymbol);
        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertTrue(game.gameIsWon());
    }

    @Test
    public void testGameIsWon_NonWinningBoardReturnsFalse() {
        String playerOneSymbol = "O";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(8).setValue(playerOneSymbol);
        Game game = TestBuilderMethods.buildGame(new Board(boardCells), null, null, null, null, null);

        assertFalse(game.gameIsWon());
    }
}
