import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;

public class BoardTest {
    @Test
    public void testUpdateCell_UpdatesBoardCellArrayWithNewValue() {
        Board board = new Board(TestBuilderMethods.buildBoardCells());

        board.updateCell(0, "O");
        board.updateCell(4, "X");

        assertEquals("O", board.getBoard().get(0).getValue());
        assertEquals("X", board.getBoard().get(4).getValue());
    }

    @Test
    public void testGetBoard_ReturnsBoardCells() {
        Board board = new Board(TestBuilderMethods.buildBoardCells());
        List<Cell> boardCells = board.getBoard();

        assertEquals(boardCells, board.getBoard());
    }

    @Test
    public void testGetBoardCell_ReturnsValueOfCell() {
        Board board = new Board(TestBuilderMethods.buildBoardCells());

        assertEquals("5", board.getBoardCell(4).getValue());
    }

    @Test
    public void testGetRowColumnCount_ReturnsTheCorrectNumberOfRowsAndColumns() {
        Board board = new Board(TestBuilderMethods.buildBoardCells());

        assertEquals(3, board.getRowColumnCount());
    }

    @Test
    public void testGetRows_ReturnsAListOfCellsWithAllRows() {
        Board board = new Board(TestBuilderMethods.buildBoardCells());

        List<List<Cell>> rows = board.getRows();
        int increment = 1;

        for(List<Cell> row : rows) {
            for(Cell cell : row) {
                assertEquals(Integer.toString(increment), cell.getValue());
                increment++;
            }
        }
    }

    @Test
    public void testGetColumns_ReturnsAListOfCellsWithAllColumns() {
        Board board = new Board(TestBuilderMethods.buildBoardCells());

        List<List<Cell>> columns = board.getColumns();
        int increment;
        int outerIncrement = 0;

        for(List<Cell> column : columns) {
            outerIncrement++;
            increment = outerIncrement;
            for(Cell cell : column) {
                assertEquals(Integer.toString(increment), cell.getValue());
                increment += 3;
            }
        }
    }

    @Test
    public void testGetDiagonals_ReturnsAListOfCellsWithAllDiagonals() {
        Board board = new Board(TestBuilderMethods.buildBoardCells());

        List<List<Cell>> diagonals = board.getDiagonals();

        int increment = 1;
        List<Cell> leftDiagonal = diagonals.get(0);
        for(Cell cell : leftDiagonal) {
            assertEquals(Integer.toString(increment), cell.getValue());
            increment += 4;
        }

        increment = 7;
        List<Cell> rightDiagonal = diagonals.get(1);
        for(Cell cell : rightDiagonal) {
            assertEquals(Integer.toString(increment), cell.getValue());
            increment -= 2;
        }
    }

    @Test
    public void testDeepClone_ReturnsADeepCopyOfBoardObject() {
        Board board = new Board(TestBuilderMethods.buildBoardCells());
        Board boardCopy = board.deepClone();
        List<Cell> boardCells = board.getBoard();
        List<Cell> boardCopyCells = boardCopy.getBoard();
        int boardSize = boardCells.size();

        assertNotSame(board, boardCopy);

        for (int i = 0; i < boardSize; i++) {
            assertEquals(boardCells.get(i).getValue(), boardCopyCells.get(i).getValue());
            assertEquals(boardCells.get(i).getX(), boardCopyCells.get(i).getX());
            assertEquals(boardCells.get(i).getY(), boardCopyCells.get(i).getY());
        }
    }
}
