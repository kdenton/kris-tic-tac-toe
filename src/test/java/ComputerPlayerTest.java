import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ComputerPlayerTest {
    @Test
    public void testCheckColumnWin_ReturnsFalseIfCannotWin() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(3).setValue(playerTwoSymbol);
        boardCells.get(1).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertFalse(computer.checkColumnWin(playerOneSymbol));
    }

    @Test
    public void testCheckColumnWin_ReturnsTrueIfCanWin() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(3).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertTrue(computer.checkColumnWin(playerOneSymbol));
        assertEquals(boardCells.get(6), computer.getNextMove());
    }

    @Test
    public void testRowColumnWin_ReturnsFalseIfCannotWin() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerTwoSymbol);
        boardCells.get(3).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertFalse(computer.checkRowWin(playerOneSymbol));
    }

    @Test
    public void testRowColumnWin_ReturnsTrueIfCanWin() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerOneSymbol);
        boardCells.get(3).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertTrue(computer.checkRowWin(playerOneSymbol));
        assertEquals(boardCells.get(2), computer.getNextMove());
    }

    @Test
    public void testCheckDiagonalWin_ReturnsFalseIfCannotWinRightDiagonal() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(6).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerTwoSymbol);
        boardCells.get(1).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertFalse(computer.checkDiagonalWin(playerOneSymbol));
    }

    @Test
    public void testCheckDiagonalWin_ReturnsTrueIfCanWinRightDiagonal() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(6).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertTrue(computer.checkDiagonalWin(playerOneSymbol));
        assertEquals(boardCells.get(2), computer.getNextMove());
    }

    @Test
    public void testCheckDiagonalWin_ReturnsFalseIfCannotWinLeftDiagonal() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerTwoSymbol);
        boardCells.get(1).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertFalse(computer.checkDiagonalWin(playerOneSymbol));
    }

    @Test
    public void testCheckDiagonalWin_ReturnsTrueIfCanWinLeftDiagonal() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertTrue(computer.checkDiagonalWin(playerOneSymbol));
        assertEquals(boardCells.get(8), computer.getNextMove());
    }

    @Test
    public void testCheckWin_IfThereIsNotAWinReturnsFalse() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(5).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertFalse(computer.checkWin());
    }

    @Test
    public void testCheckWin_IfThereIsAWinReturnsTrue() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertTrue(computer.checkWin());
        assertEquals(boardCells.get(8), computer.getNextMove());
    }

    @Test
    public void testCheckBlock_IfThereIsNotABlockReturnsFalse() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerTwoSymbol);
        boardCells.get(5).setValue(playerTwoSymbol);
        boardCells.get(1).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertFalse(computer.checkBlock());
    }

    @Test
    public void testCheckBlock_IfThereIsABlockReturnsTrue() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerTwoSymbol);
        boardCells.get(4).setValue(playerTwoSymbol);
        boardCells.get(1).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertTrue(computer.checkBlock());
        assertEquals(boardCells.get(8), computer.getNextMove());
    }

    @Test
    public void testCheckFork_IfNotAbleToForkReturnsFalse() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(2).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertFalse(computer.checkFork());
    }

    @Test
    public void testCheckFork_IfAbleToForkReturnsTrue() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerOneSymbol);
        boardCells.get(8).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertTrue(computer.checkFork());
        assertEquals(boardCells.get(6), computer.getNextMove());
    }

    @Test
    public void testCheckBlockFork_IfNotAbleToBlockForkReturnsFalse() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(2).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertFalse(computer.checkBlockFork());
    }

    @Test
    public void testCheckBlockFork_IfAbleToBlockForkReturnsTrue() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerTwoSymbol);
        boardCells.get(8).setValue(playerTwoSymbol);
        boardCells.get(1).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertTrue(computer.checkBlockFork());
        assertEquals(boardCells.get(6), computer.getNextMove());
    }

    @Test
    public void testCheckTwoInRow_IfWouldCauseForkReturnsFalse() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerTwoSymbol);
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(6).setValue(playerTwoSymbol);
        boardCells.get(8).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertFalse(computer.checkTwoInRow());
    }

    @Test
    public void testCheckTwoInRow_IfCanGetTwoInRowReturnsTrue() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerTwoSymbol);
        boardCells.get(8).setValue(playerTwoSymbol);
        boardCells.get(5).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertTrue(computer.checkTwoInRow());
        assertEquals(boardCells.get(4), computer.getNextMove());
    }

    @Test
    public void testCheckCenter_IfCenterIsNotEmptyReturnsFalse() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertFalse(computer.checkCenter());
    }

    @Test
    public void testCheckCenter_IfCenterIsEmptyReturnsTrue() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerOneSymbol);
        boardCells.get(8).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertTrue(computer.checkCenter());
        assertEquals(boardCells.get(4), computer.getNextMove());
    }

    @Test
    public void testCheckOppositeCorner_IfNoOppositeCornerReturnsFalse() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerOneSymbol);
        boardCells.get(8).setValue(playerOneSymbol);
        boardCells.get(6).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertFalse(computer.checkOppositeCorner());
    }

    @Test
    public void testCheckOppositeCorner_IfOppositeCornerReturnsTrue() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerTwoSymbol);
        boardCells.get(5).setValue(playerTwoSymbol);
        boardCells.get(4).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertTrue(computer.checkOppositeCorner());
        assertEquals(boardCells.get(6).getValue(), computer.getNextMove().getValue());
    }

    @Test
    public void testCheckEmptyCorner_IfNotEmptyCornerReturnsFalse() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerTwoSymbol);
        boardCells.get(2).setValue(playerTwoSymbol);
        boardCells.get(6).setValue(playerOneSymbol);
        boardCells.get(8).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertFalse(computer.checkEmptyCorner());
    }

    @Test
    public void testCheckEmptyCorner_IfEmptyCornerReturnsTrue() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerTwoSymbol);
        boardCells.get(2).setValue(playerTwoSymbol);
        boardCells.get(6).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertTrue(computer.checkEmptyCorner());
        assertEquals(boardCells.get(8), computer.getNextMove());
    }

    @Test
    public void testCheckEmptySide_IfNoEmptyMiddleSquareOnAnySideReturnsFalse() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(1).setValue(playerTwoSymbol);
        boardCells.get(3).setValue(playerTwoSymbol);
        boardCells.get(5).setValue(playerOneSymbol);
        boardCells.get(7).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertFalse(computer.checkEmptySide());
    }

    @Test
    public void testCheckEmptySide_IfEmptyMiddleSquareOnAnySideReturnsTrue() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerTwoSymbol);
        boardCells.get(3).setValue(playerTwoSymbol);
        boardCells.get(5).setValue(playerOneSymbol);
        boardCells.get(7).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        assertTrue(computer.checkEmptySide());
        assertEquals(boardCells.get(1), computer.getNextMove());
    }

    @Test
    public void testMakeNextMove_UpdatesBoard() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        Board board = new Board(boardCells);
        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        computer.makeNextMove(0);

        assertEquals(playerOneSymbol, boardCells.get(0).getValue());
    }

    @Test
    public void testCheckPossibleTwoInColumn_IfPossibleReturnsNearbyCells() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerOneSymbol);
        boardCells.get(5).setValue(playerOneSymbol);
        boardCells.get(7).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        List<Cell> possibleCells = computer.checkPossibleTwoInColumn();

        assertEquals(3, possibleCells.size());
        assertEquals(boardCells.get(3), possibleCells.get(0));
        assertEquals(boardCells.get(2), possibleCells.get(1));
        assertEquals(boardCells.get(8), possibleCells.get(2));
    }

    @Test
    public void testCheckPossibleTwoInColumn_IfNotPossibleReturnsEmptyList() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerTwoSymbol);
        boardCells.get(4).setValue(playerTwoSymbol);
        boardCells.get(5).setValue(playerOneSymbol);
        boardCells.get(7).setValue(playerOneSymbol);
        boardCells.get(8).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        List<Cell> possibleCells = computer.checkPossibleTwoInColumn();

        assertEquals(0, possibleCells.size());
    }

    @Test
    public void testCheckPossibleTwoInRow_IfPossibleReturnsNearbyCells() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(1).setValue(playerTwoSymbol);
        boardCells.get(4).setValue(playerOneSymbol);
        boardCells.get(8).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        List<Cell> possibleCells = computer.checkPossibleTwoInRow();

        assertEquals(2, possibleCells.size());
        assertEquals(boardCells.get(3), possibleCells.get(0));
        assertEquals(boardCells.get(5), possibleCells.get(1));
    }

    @Test
    public void testCheckPossibleTwoInRow_IfNotPossibleReturnsEmptyList() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerTwoSymbol);
        boardCells.get(4).setValue(playerTwoSymbol);
        boardCells.get(5).setValue(playerOneSymbol);
        boardCells.get(7).setValue(playerOneSymbol);
        boardCells.get(8).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        List<Cell> possibleCells = computer.checkPossibleTwoInRow();

        assertEquals(0, possibleCells.size());
    }

    @Test
    public void testCheckPossibleTwoInDiagonal_LeftDiagonalIfPossibleReturnsNearbyCells() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        List<Cell> possibleCells = computer.checkPossibleTwoInDiagonal();

        assertEquals(1, possibleCells.size());
        assertEquals(boardCells.get(4), possibleCells.get(0));
    }

    @Test
    public void testCheckPossibleTwoInDiagonal_LeftDiagonalIfNotPossibleReturnsEmptyList() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(0).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        List<Cell> possibleCells = computer.checkPossibleTwoInDiagonal();

        assertEquals(0, possibleCells.size());
    }

    @Test
    public void testCheckPossibleTwoInDiagonal_RightDiagonalIfPossibleReturnsNearbyCells() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerOneSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        List<Cell> possibleCells = computer.checkPossibleTwoInDiagonal();

        assertEquals(1, possibleCells.size());
        assertEquals(boardCells.get(4), possibleCells.get(0));
    }

    @Test
    public void testCheckPossibleTwoInDiagonal_RightDiagonalIfNotPossibleReturnsEmptyList() {
        String playerOneSymbol = "O";
        String playerTwoSymbol = "X";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        boardCells.get(2).setValue(playerOneSymbol);
        boardCells.get(4).setValue(playerTwoSymbol);
        Board board = new Board(boardCells);

        ComputerPlayer computer = new ComputerPlayer(playerOneSymbol, playerTwoSymbol, board);

        List<Cell> possibleCells = computer.checkPossibleTwoInDiagonal();

        assertEquals(0, possibleCells.size());
    }
}
