import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class GameAcceptanceTest {
    @Test
    public void test() {
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        String inputs = "1\n7\n6\n8\n2";
        Board board = new Board(boardCells);
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        InputStream input = new ByteArrayInputStream(inputs.getBytes());
        ReadsInput reader = new Reader(input, presenter);
        Player human = new HumanPlayer("O", board, reader);
        Player computer = new ComputerPlayer("X", "O", board);
        ManagesBoard boardManager = new BoardManager(board);
        PlaysGame game = new Game(board, presenter, human, computer, boardManager);
        GameRunner runner = new GameRunner(game);

        runner.runGame();
    }

    @Test
    public void testPlay_TieGameAllTheWayThrough() {
        String inputs = "1\n2\n6\n3\n5\n4\n7\n9\n8";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        Board board = new Board(boardCells);
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        ReadsInput reader = new Reader(new ByteArrayInputStream(inputs.getBytes()), presenter);
        Player playerOne = TestBuilderMethods.buildHumanPlayer("O", board, presenter, reader);
        Player playerTwo = TestBuilderMethods.buildHumanPlayer("X", board, presenter, reader);
        ManagesBoard boardManager = new BoardManager(board);
        Game game = new Game(board, presenter, playerOne, playerTwo, boardManager);
        GameRunner runner = new GameRunner(game);

        runner.runGame();

        String expectedOutput = BuildAcceptanceTestOutput(inputs, Messages.GameIsTied);
        assertEquals(expectedOutput, output.toString());
    }

    @Test
    public void testPlay_XWinAllTheWayThrough() {
        String inputs = "1\n5\n2\n4\n6\n3\n9\n7";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        Board board = new Board(boardCells);
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        ReadsInput reader = new Reader(new ByteArrayInputStream(inputs.getBytes()), presenter);
        Player playerOne = TestBuilderMethods.buildHumanPlayer("O", board, presenter, reader);
        Player playerTwo = TestBuilderMethods.buildHumanPlayer("X", board, presenter, reader);
        ManagesBoard boardManager = new BoardManager(board);
        Game game = new Game(board, presenter, playerOne, playerTwo, boardManager);
        GameRunner runner = new GameRunner(game);

        runner.runGame();

        String expectedOutput = BuildAcceptanceTestOutput(inputs, Messages.GameIsWon(playerTwo.getPlayerSymbol()));
        assertEquals(expectedOutput, output.toString());
    }

    @Test
    public void testPlay_OWinAllTheWayThrough() {
        String inputs = "5\n2\n7\n3\n1\n9\n6\n8\n4";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        Board board = new Board(boardCells);
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        ReadsInput reader = new Reader(new ByteArrayInputStream(inputs.getBytes()), presenter);
        Player playerOne = TestBuilderMethods.buildHumanPlayer("O", board, presenter, reader);
        Player playerTwo = TestBuilderMethods.buildHumanPlayer("X", board, presenter, reader);
        ManagesBoard boardManager = new BoardManager(board);
        Game game = new Game(board, presenter, playerOne, playerTwo, boardManager);
        GameRunner runner = new GameRunner(game);

        runner.runGame();

        String expectedOutput = BuildAcceptanceTestOutput(inputs, Messages.GameIsWon(playerOne.getPlayerSymbol()));
        assertEquals(expectedOutput, output.toString());
    }

    @Test
    public void testPlay_InvalidCellIdAsksForInputAgain() {
        String inputs = "5\n10\n2\n7\n3\n1\n9\n6\n8\n4";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        Board board = new Board(boardCells);
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        ReadsInput reader = new Reader(new ByteArrayInputStream(inputs.getBytes()), presenter);
        Player playerOne = TestBuilderMethods.buildHumanPlayer("O", board, presenter, reader);
        Player playerTwo = TestBuilderMethods.buildHumanPlayer("X", board, presenter, reader);
        ManagesBoard boardManager = new BoardManager(board);
        Game game = new Game(board, presenter, playerOne, playerTwo, boardManager);
        GameRunner runner = new GameRunner(game);

        runner.runGame();

        String expectedOutput = BuildAcceptanceTestOutput(inputs, Messages.GameIsWon(playerOne.getPlayerSymbol()));
        assertEquals(expectedOutput, output.toString());
    }

    @Test
    public void testPlay_AlreadyTakenCellAsksForInputAgain() {
        String inputs = "5\n2\n7\n3\n1\n9\n9\n6\n8\n4";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        Board board = new Board(boardCells);
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        ReadsInput reader = new Reader(new ByteArrayInputStream(inputs.getBytes()), presenter);
        Player playerOne = TestBuilderMethods.buildHumanPlayer("O", board, presenter, reader);
        Player playerTwo = TestBuilderMethods.buildHumanPlayer("X", board, presenter, reader);
        ManagesBoard boardManager = new BoardManager(board);
        Game game = new Game(board, presenter, playerOne, playerTwo, boardManager);
        GameRunner runner = new GameRunner(game);

        runner.runGame();

        String expectedOutput = BuildAcceptanceTestOutput(inputs, Messages.GameIsWon(playerOne.getPlayerSymbol()));
        assertEquals(expectedOutput, output.toString());
    }

    @Test
    public void testPlay_NonIntegerInputAsksForInputAgain() {
        String inputs = "5\n2\ny\n7\n3\n1\n9\n9\n6\n8\n4";
        List<Cell> boardCells = TestBuilderMethods.buildBoardCells();
        Board board = new Board(boardCells);
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        ReadsInput reader = new Reader(new ByteArrayInputStream(inputs.getBytes()), presenter);
        Player playerOne = TestBuilderMethods.buildHumanPlayer("O", board, presenter, reader);
        Player playerTwo = TestBuilderMethods.buildHumanPlayer("X", board, presenter, reader);
        ManagesBoard boardManager = new BoardManager(board);
        Game game = new Game(board, presenter, playerOne, playerTwo, boardManager);
        GameRunner runner = new GameRunner(game);

        runner.runGame();

        String expectedOutput = BuildAcceptanceTestOutput(inputs, Messages.GameIsWon(playerOne.getPlayerSymbol()));
        assertEquals(expectedOutput, output.toString());
    }

private String BuildAcceptanceTestOutput(String input, String endingMessage) {
    List<Cell> boardState = TestBuilderMethods.buildBoardCells();
    Player playerOne = TestBuilderMethods.buildHumanPlayer("O", null, null, null);
    Player playerTwo = TestBuilderMethods.buildHumanPlayer("X", null, null, null);
    String[] inputs = input.split("\\r?\\n");
    StringBuilder builder = new StringBuilder();
    Player currentPlayer = playerTwo;

    for (String s: inputs) {
        builder.append(TestBuilderMethods.generateBoardForTest(boardState));
        currentPlayer = currentPlayer == playerOne ? playerTwo : playerOne;
        builder.append(Messages.PlayerTurnPrompt(currentPlayer.getPlayerSymbol()));

        int cellToReplace;
        try {
            cellToReplace = Integer.parseInt(s) - 1;
        }
        catch (NumberFormatException e) {
            builder.append(Messages.WrongInputForId);
            currentPlayer = currentPlayer == playerOne ? playerTwo : playerOne;
            continue;
        }

        if (cellToReplace < 0 || cellToReplace > 8) {
            builder.append(Messages.CellIdOutOfBounds);
            currentPlayer = currentPlayer == playerOne ? playerTwo : playerOne;
            continue;
        }

        if (boardState.get(cellToReplace).getValue().equals(playerOne.getPlayerSymbol()) ||
                boardState.get(cellToReplace).getValue().equals(playerTwo.getPlayerSymbol())) {
            builder.append(Messages.CellTaken);
            currentPlayer = currentPlayer == playerOne ? playerTwo : playerOne;
            continue;
        }

        Cell cell = boardState.get(cellToReplace);
        cell.setValue(currentPlayer.getPlayerSymbol());
        boardState.set(cellToReplace, cell);
    }

    builder.append(TestBuilderMethods.generateBoardForTest(boardState));
    builder.append(endingMessage);

    return builder.toString();
}
}
