import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import static junit.framework.TestCase.assertEquals;

public class PresenterTest {
    @Test
    public void tesDisplayOutput_DisplaysProvidedString() {
        final String stringToPrint = "test";
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);

        presenter.displayOutput(stringToPrint);

        assertEquals(stringToPrint, output.toString());
    }
}
