import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.LinkedList;
import java.util.List;

public class TestBuilderMethods {
    static Game buildGame(Board board, Player playerOne, Player playerTwo, ManagesBoard boardManager,
                          PresentsOutput presenter, ReadsInput reader) {
        board = board != null ? board : new Board(TestBuilderMethods.buildBoardCells());
        presenter = presenter != null ? presenter : new Presenter(new ByteArrayOutputStream());
        reader = reader != null ? reader : new Reader(new ByteArrayInputStream("1".getBytes()), presenter);
        playerOne = playerOne != null ? playerOne : buildHumanPlayer("O", board, presenter, reader);
        playerTwo = playerTwo != null ? playerTwo : buildHumanPlayer("X", board, presenter, reader);
        boardManager = boardManager != null ? boardManager : new BoardManager(board);

        return new Game(board, presenter, playerOne, playerTwo, boardManager);
    }

    static HumanPlayer buildHumanPlayer(String symbol, Board board, PresentsOutput presenter, ReadsInput reader) {
        board = board != null ? board : new Board(TestBuilderMethods.buildBoardCells());
        presenter = presenter != null ? presenter : new Presenter(new ByteArrayOutputStream());
        reader = reader != null ? reader : new Reader(new ByteArrayInputStream("1".getBytes()), presenter);

        return new HumanPlayer(symbol, board, reader);
    }

    static ComputerPlayer buildComputerPlayer(String symbol, String opponentSymbol, Board board) {
        symbol = symbol != null ? symbol : "X";
        opponentSymbol = opponentSymbol != null ? opponentSymbol : "O";
        board = board != null ? board : new Board(TestBuilderMethods.buildBoardCells());


        return new ComputerPlayer(symbol, opponentSymbol, board);
    }

    static List<Cell> buildBoardCells() {
        List<Cell> cells = new LinkedList<>();
        int counter = 0;

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                counter++;
                Cell cellToAdd = new Cell();
                cellToAdd.setValue(Integer.toString(counter));
                cellToAdd.setX(i);
                cellToAdd.setY(j);

                cells.add(cellToAdd);
            }
        }

        return cells;
    }

    static String generateBoardForTest(List<Cell> boardCells){
        Board board = new Board(boardCells);
        List<List<Cell>> rows = board.getRows();
        StringBuilder sb  = new StringBuilder();

        sb.append("\n   |   |   \n");
        for (int i = 0; i < rows.size(); i++) {
            if (i != 0) {
                sb.append("___|___|___\n");
                sb.append("   |   |   \n");
            }

            List<Cell> row = rows.get(i);
            StringBuilder set = new StringBuilder(" ");

            for (int j = 0; j < row.size(); j++) {
                if (j != 0) {
                    set.append("| ");
                }

                set.append(row.get(j).getValue()).append(" ");
            }
            set.append("\n");
            sb.append(set);
        }
        sb.append("   |   |   \n");

        return sb.toString();
    }
}
