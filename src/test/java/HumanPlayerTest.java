import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;

import static junit.framework.TestCase.assertEquals;

public class HumanPlayerTest {
    @Test
    public void testGetPlayerSymbol_ReturnsCorrectSymbol() {
        Board board = new Board(TestBuilderMethods.buildBoardCells());
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        ReadsInput reader = new Reader(new ByteArrayInputStream("1".getBytes()), presenter);
        Player player = new HumanPlayer("O", board, reader);
        assertEquals("O", player.getPlayerSymbol());
    }

    @Test
    public void testDecideNextMove_ReturnsCorrectIndexFromInput() {
        Board board = new Board(TestBuilderMethods.buildBoardCells());
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        ReadsInput reader = new Reader(new ByteArrayInputStream("4".getBytes()), presenter);
        Player player = new HumanPlayer("O", board, reader);

        assertEquals(3, player.decideNextMove());
    }

    @Test
    public void testDecideNextMove_IfInvalidInputReturnsNegativeOne() {
        Board board = new Board(TestBuilderMethods.buildBoardCells());
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        ReadsInput reader = new Reader(new ByteArrayInputStream("z".getBytes()), presenter);
        Player player = new HumanPlayer("O", board, reader);

        assertEquals(-1, player.decideNextMove());
    }

    @Test
    public void testMakeNextMove_UpdatesTheBoardCorrectly() {
        Board board = new Board(TestBuilderMethods.buildBoardCells());
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        ReadsInput reader = new Reader(new ByteArrayInputStream("z".getBytes()), presenter);
        Player player = new HumanPlayer("O", board, reader);

        player.makeNextMove(3);

        assertEquals("O", board.getBoard().get(3).getValue());
    }
}
