import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class MessagesTest {
    @Test
    public void testPlayerTurnPrompt_ReturnsPromptWithCorrectSymbol() {
        String expectedPrompt = "\nPlayer X's Turn\nEnter cell to place your X: ";
        assertEquals(expectedPrompt, Messages.PlayerTurnPrompt("X"));
    }

    @Test
    public void testGameIsWon_ReturnsWinMessageWithCorrectSymbol() {
        String expectedMessage = "\nThe O player has won the game!";
        assertEquals(expectedMessage, Messages.GameIsWon("O"));
    }
}
