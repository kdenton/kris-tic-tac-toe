import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import static org.junit.Assert.assertEquals;

public class ReaderTest {
    @Test
    public void testReadIntInput_IfNextInputIsIntItReturnsThatValue() {
        final int intToReceive = 1;
        InputStream input = new ByteArrayInputStream("1".getBytes());
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        ReadsInput reader = new Reader(input, presenter);

        assertEquals(intToReceive, reader.readIntInput());
    }

    @Test
    public void testReadIntInput_IfNextInputIsNotIntItReturnsNeg1AndPrintsError() {
        final int intToReceive = -1;
        InputStream input = new ByteArrayInputStream("a".getBytes());
        OutputStream output = new ByteArrayOutputStream();
        PresentsOutput presenter = new Presenter(output);
        ReadsInput reader = new Reader(input, presenter);

        assertEquals(intToReceive, reader.readIntInput());
        assertEquals(Messages.WrongInputForId, output.toString());
    }
}
